package monk;

import java.util.*;
import java.io.*;
public class ChamberOfSecret {
	
     Scanner inputNX = new Scanner (System.in);
	 
     public static class CircularQueue
     {
		
		private int queue[];
		private int queueCapacity,count;
		int front,end;
		
		public CircularQueue(int queueCapacity) 
		{
			this.queueCapacity=queueCapacity;
			front=end=-1;
			queue=new int[queueCapacity];
			count=0;
		}
		
		
		public void InsertIntoQueue(int data)
		{
			if(count==queueCapacity)
				return;
			
			if(front==-1)
			{
				queue[++front]=data;
				end=front;
			}
			else
			{
				end=(++end)%queueCapacity;
				queue[end]=data;
			}
					
				count+=1;
			
			}
		
		public int removeFromQueue()
		{
			int data=-1;
			
			if(count!=0)
			{
				data=queue[front];
				front=(++front)%queueCapacity;
				count-=1;
			}
			
			return data;
		}
  
    }


    public int ArgogInput()
    {
   	   int in = inputNX.nextInt();
   	   return in;
    }
    
    
    public void InsertPowersAndIndexIntoQueue(int totalSpiders ,ChamberOfSecret.CircularQueue spiders, ChamberOfSecret.CircularQueue spiderIndex ) throws InstantiationException, IllegalAccessException
    {
    	
        for(int InputSpiderandIndex=0 ; InputSpiderandIndex < totalSpiders ; InputSpiderandIndex++)
        {
	        spiders.InsertIntoQueue(ArgogInput());
	        spiderIndex.InsertIntoQueue(InputSpiderandIndex+1);
        }    
        
    }
    
    
    public int[] DequeuingAndGettingMaxIndexAndPointer(int NumberOfSpiderstobeSelected , ChamberOfSecret.CircularQueue spiders, ChamberOfSecret.CircularQueue spiderIndex , int[] dequeuedspidersArray ,int[] dequeuedArrayspiderIndex)
    {
       int maxPower = -1, maxIndex=0 ,ParseSpiderandIndex ;
    	
	     for( ParseSpiderandIndex=0 ; ParseSpiderandIndex < NumberOfSpiderstobeSelected ; ParseSpiderandIndex++)
	     {
	    	int spiderPower=spiders.removeFromQueue();
		    int spiderPowerIndex=spiderIndex.removeFromQueue();
		
	    	dequeuedspidersArray[ParseSpiderandIndex]=spiderPower;
		    dequeuedArrayspiderIndex[ParseSpiderandIndex]=spiderPowerIndex;
		
		    if(spiderPower==-1)
			    break;
		
	    	if(spiderPower>maxPower)
		    {
			    maxPower=spiderPower;
			    maxIndex=spiderPowerIndex;
		    }
    	
	      }
	      return new int[]{ maxIndex ,ParseSpiderandIndex };
    }
    
    
        
    
    public void insertBackToQueue(int[] dequeuedArrayspiderIndex ,int[] dequeuedspidersArray , int Index , int maxIndex , ChamberOfSecret.CircularQueue spiders, ChamberOfSecret.CircularQueue spiderIndex )
    {
       	 for(int PutBackQueueIndex=0; PutBackQueueIndex < Index ; PutBackQueueIndex++ )
	     {
	       if(dequeuedArrayspiderIndex[PutBackQueueIndex]!=maxIndex)
		   {
			  spiders.InsertIntoQueue(dequeuedspidersArray[PutBackQueueIndex]>0?dequeuedspidersArray[PutBackQueueIndex]-1:0);
			  spiderIndex.InsertIntoQueue(dequeuedArrayspiderIndex[PutBackQueueIndex]);
		   }
	    
	     }	
    	
    }
    
    
    
    
    public void ToFindFirstXSelectedSpidersInorder(int NumberOfSpiderstobeSelected , ChamberOfSecret.CircularQueue spiders, ChamberOfSecret.CircularQueue spiderIndex , int[] indexArrayOfMaxPowerSpider)
    {
    	  for(int ChooseTopSpider=0 ; ChooseTopSpider < NumberOfSpiderstobeSelected ; ChooseTopSpider++)
	      {
		        
		     int dequeuedspidersArray[]=new int[NumberOfSpiderstobeSelected];
		     int dequeuedArrayspiderIndex[]=new int[NumberOfSpiderstobeSelected];
		        
		     int maxIndex[] = DequeuingAndGettingMaxIndexAndPointer(NumberOfSpiderstobeSelected , spiders , spiderIndex ,dequeuedspidersArray , dequeuedArrayspiderIndex );
	
			 indexArrayOfMaxPowerSpider[ChooseTopSpider] = maxIndex[0];
					
		     insertBackToQueue(dequeuedArrayspiderIndex ,dequeuedspidersArray ,  maxIndex[1],  maxIndex[0] ,  spiders, spiderIndex );
            
	      }
	
     }
    
    
    
    public void printArray(int NumberOfSpiderstobeSelected ,  int[] indexArrayOfMaxPowerSpider)
    {
    	
    	 for(int ChooseTopSpider=0; ChooseTopSpider < NumberOfSpiderstobeSelected; ChooseTopSpider++)
         { 
            System.out.print(indexArrayOfMaxPowerSpider[ChooseTopSpider] + " ");
         }
    	 
    }


          
    public static void main(String[] args) throws IOException, InstantiationException, IllegalAccessException
    {   
	ChamberOfSecret Argog = new ChamberOfSecret();
	final int MaxNumberOfSpiderscanbeSelected = 316;
  	int NumberOfSpiders= Argog.ArgogInput();
   	int NumberOfSpiderstobeSelected = Argog.ArgogInput(); 
   		   		 		
   	if( NumberOfSpiderstobeSelected < 1 || NumberOfSpiderstobeSelected > MaxNumberOfSpiderscanbeSelected || NumberOfSpiders < NumberOfSpiderstobeSelected || NumberOfSpiders > NumberOfSpiderstobeSelected * NumberOfSpiderstobeSelected )
   	{  
   		Argog.inputNX.close();
   		return ;
   	}
   		
   	else 
   	{
            int indexArrayOfMaxPowerSpider[] = new int[NumberOfSpiderstobeSelected]; 
            CircularQueue spiders=new CircularQueue(NumberOfSpiders);
	    CircularQueue spiderIndex=new CircularQueue(NumberOfSpiders);
	    Argog.InsertPowersAndIndexIntoQueue(NumberOfSpiders , spiders , spiderIndex);
            Argog.ToFindFirstXSelectedSpidersInorder(NumberOfSpiderstobeSelected , spiders,  spiderIndex , indexArrayOfMaxPowerSpider);
            Argog.printArray( NumberOfSpiderstobeSelected , indexArrayOfMaxPowerSpider);
	         
   	}
	  
   }
      
		
}
 
 



 


