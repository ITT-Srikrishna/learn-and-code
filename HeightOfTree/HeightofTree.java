import java.util.*;



class HeightofTree {
	static TreeNode rootNodeOfTree;
	public static Scanner sc = new Scanner(System.in);


	HeightofTree() {
		rootNodeOfTree = null;
	}

	void insertElementIntoTree(int nodeValue) {
		rootNodeOfTree = insertAccordingToNodeValue(rootNodeOfTree, nodeValue);
	}

	TreeNode insertAccordingToNodeValue(TreeNode rootNode, int nodeValue) {

		if (rootNode == null) {
			rootNode = new TreeNode(nodeValue);
			return rootNode;
		}

		if (nodeValue < rootNode.nodeValue)
			rootNode.leftChild = insertAccordingToNodeValue(rootNode.leftChild, nodeValue);
		else if (nodeValue > rootNode.nodeValue)
			rootNode.rightChild = insertAccordingToNodeValue(rootNode.rightChild, nodeValue);

		return rootNode;
	}

	public int heightOfBinaryTree(TreeNode node) {
		if (node == null) {
			return 0;
		} else {
			return 1 + Math.max(heightOfBinaryTree(node.leftChild), heightOfBinaryTree(node.rightChild));
		}
	}

	public static void main(String[] args) {
		HeightofTree tree = new HeightofTree();

		int numberOfElements = sc.nextInt();
		for (int nodeNumber = 0; nodeNumber < numberOfElements; nodeNumber++) {

			tree.insertElementIntoTree(sc.nextInt());
		}
		System.out.println(tree.heightOfBinaryTree(rootNodeOfTree));
	}
}
// T