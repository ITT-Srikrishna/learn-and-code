class TreeNode {
	int nodeValue;
	TreeNode leftChild, rightChild;

	public TreeNode(int item) {
		nodeValue = item;
		leftChild = rightChild = null;
	}

}