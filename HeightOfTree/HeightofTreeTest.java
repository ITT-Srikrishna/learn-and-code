import static org.junit.Assert.*;
import org.junit.Test;


public class HeightofTreeTest {

	@Test
	public void testInsertAccordingToNodeValue() {
		HeightofTree ht = new HeightofTree();
		TreeNode tn = new TreeNode(5);
		TreeNode rn = ht.insertAccordingToNodeValue(null,5);
		assertNotNull(rn);
	}
	
	@Test
	public void testInsertLeftAccordingToNodeValue() {
		HeightofTree ht = new HeightofTree();
		TreeNode root = null;
		TreeNode tn = ht.insertAccordingToNodeValue(root,5);
		tn = ht.insertAccordingToNodeValue(tn, 4);
		assertEquals(4,tn.leftChild.nodeValue);
	}
	
	@Test
	public void testInsertRightAccordingToNodeValue() {
		HeightofTree ht = new HeightofTree();
		TreeNode root = null;
		TreeNode tn = ht.insertAccordingToNodeValue(root,5);
		tn = ht.insertAccordingToNodeValue(tn, 4);
		assertEquals(4,tn.rightChild.nodeValue);
	}
	
	@Test
	public void testHeightOfBinaryTree() {
		
		
	}

}
