import java.util.*;

public class Statistics {
	List<String> numberList;
	String maxCountGame;
	int maxCount = 0;
	HashMap<String, List<String>> hashmap = new HashMap<String, List<String>>();
	Scanner enterInput = new Scanner(System.in);
	String[] inputs = new String[2];

	public void inputPersonAndHisGameOfInterest() {
		String Line = enterInput.nextLine();
		inputs[0] = Line.substring(0, Line.indexOf(' '));
		inputs[1] = Line.substring(Line.indexOf(' ') + 1);
	}

	public void mapPersonAndHisGameIntoHashmap() {
		if (hashmap.containsKey(inputs[1])) {
			keyAlreadyPresent();

		} else {
			IfKeyisNotPresent();
		}

	}

	public void keyAlreadyPresent() {
		numberList = hashmap.get(inputs[1]);
		checkIfListContainsPersonName();

	}

	public void checkIfListContainsPersonName() {
		if (numberList.contains(inputs[0])) {
			return;
		} else {
			numberList.add(inputs[0]);
		}
	}

	public void IfKeyisNotPresent() {
		numberList = new LinkedList<String>();
		numberList.add(inputs[0]);
		hashmap.put(inputs[1], numberList);
	}


	public void parseAndGetMaxCountGameName() {
		for (HashMap.Entry<String, List<String>> entry : hashmap.entrySet()) {

			numberList = entry.getValue();
			int count = numberList.size();
			checktogetMaxCount(count, entry);

		}

		System.out.println(maxCountGame);
	}

	public void checktogetMaxCount(int count, HashMap.Entry<String, List<String>> entry) {
		System.out.println(entry.getKey() + " : " + entry.getValue());
		if (count > maxCount) {
			maxCountGame = "";
			maxCount = count;
			maxCountGame = entry.getKey();
		}
	}

	public void checkFootballersCount() {
		if (hashmap.get("football") != null) {
			System.out.println(hashmap.get("football").size());
		} else {
			System.out.println("0");
		}
	}

	public static void main(String[] args) {
		Statistics statistics = new Statistics();
		int NumberOfPeopleTobeConsidered = statistics.enterInput.nextInt();
		String junkLinetoReadEmptySpace = statistics.enterInput.nextLine();

		for (int personNumber = 0; personNumber < NumberOfPeopleTobeConsidered; personNumber++) {
			statistics.inputPersonAndHisGameOfInterest();
			statistics.mapPersonAndHisGameIntoHashmap();
		}
		statistics.parseAndGetMaxCountGameName();
		statistics.checkFootballersCount();
	/*	statistics.display();*/

	}

}
