import java.util.LinkedList;

import java.lang.Math;
import java.util.*;

public class CustomHash {


	@SuppressWarnings("unchecked")
	public LinkedList<Bucket>[] listOfBucket = new LinkedList[ARR_SIZE];
	

	public static class Bucket {
		public Object key;
		public Object value;
	}

	public CustomHash() {
		for (int bucketNumber = 0; bucketNumber < ARR_SIZE; bucketNumber++) {
			listOfBucket[bucketNumber] = null;
		}
	}

	public static final int ARR_SIZE = 128;

	public boolean ContainsKey(Object key) {
		int hashvalue = key.hashCode() % ARR_SIZE;
		if (listOfBucket[Math.abs(hashvalue)] == null) {
			return false;
		}
		return true;
	}

	public Object get(Object key) {
		Bucket bucket = getBucket(key);

		if (bucket == null)
			return null;
		else
			return bucket.value;
	}

	private Bucket getBucket(Object key) {
		if (key == null)
			return null;

		int hashvalue = key.hashCode() % ARR_SIZE;
		LinkedList<Bucket> bucket = listOfBucket[Math.abs(hashvalue)];

		if (bucket == null)
			return null;

		for (Bucket Tempbucket : bucket) {
			if (Tempbucket.key.equals(key))
				return Tempbucket;
		}

		return null;
	}

	public void put(Object key, Object value) {
		int hashvalue = key.hashCode() % ARR_SIZE;
		LinkedList<Bucket> Tempbucket = listOfBucket[Math.abs(hashvalue)];

		if (Tempbucket == null) {
			Tempbucket = new LinkedList<Bucket>();

			Bucket NewBucket = new Bucket();
			NewBucket.key = key;
			NewBucket.value = value;

			Tempbucket.add(NewBucket);

			listOfBucket[Math.abs(hashvalue)] = Tempbucket;
		} else {
			for (Bucket Bucketitem : Tempbucket) {
				if (Bucketitem.key.equals(key)) {
					Bucketitem.value = value;
					return;
				}
			}

			Bucket Bucketitem = new Bucket();
			Bucketitem.key = key;
			Bucketitem.value = value;

			Tempbucket.add(Bucketitem);
		}
	}
	
}