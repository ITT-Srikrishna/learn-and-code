import java.util.*;

public class CustomHashMap {

	public static CustomHash hm = new CustomHash();
	public static CustomHash shm = new CustomHash();
	public static Scanner enterInput = new Scanner(System.in);
	public static String[] inputs = new String[2];

	public void inputPersonAndHisGameOfInterest() {
		String Line = enterInput.nextLine();
		inputs[0] = Line.substring(0, Line.indexOf(' '));
		inputs[1] = Line.substring(Line.indexOf(' ') + 1);
	}

	public void mapPersonAndHisGameIntoHashmap() {

		hm.put(inputs[0], inputs[1]);

	}

	public void display() {
		for (int i = 0; i < hm.ARR_SIZE; i++) {
			if (hm.listOfBucket[i] == null) {
				continue;
			} else {
				System.out.println(hm.listOfBucket[i].element().key + "-->" + hm.listOfBucket[i].element().value);
			}

		}
	}

	public static void main(String args[]) {
		CustomHashMap cs = new CustomHashMap();
		int n = enterInput.nextInt();
		String Junk = enterInput.nextLine();

		for (int i = 0; i < n; i++) {
			cs.inputPersonAndHisGameOfInterest();
			cs.mapPersonAndHisGameIntoHashmap();

		}

		cs.display();

	}
}
