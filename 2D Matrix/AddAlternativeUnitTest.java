import static org.junit.Assert.*;
import org.junit.Test;


public class AddAlternativeUnitTest {

	@Test
	public void CountOfElemetsTest(int[] matrix_array) throws Exception
    {
        //Arrange
		AlternateNumbersSum calculatewithMatrix = new AlternateNumbersSum();
        int expected = 6;

        //Act
        int numberofElements = calculatewithMatrix.CountOfElemets(matrix_array);

        //Assert
        assertEquals(expected, numberofElements);
    }
	
	@Test
	public void IsValidInputTest(int num) throws Exception
    {
        //Arrange
		AlternateNumbersSum calculatewithMatrix = new AlternateNumbersSum();
        Boolean expected = true;

        //Act
        Boolean isValid = calculatewithMatrix.IsValidInput(6);

        //Assert
        assertEquals(expected, isValid);
    }

}
