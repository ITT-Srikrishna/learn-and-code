
public interface AddAlternativeInterface {
		
		String GetInput();
        Boolean IsValidInput(int num);
        int CountOfElemets(int[] matrix_array);
        int[] SplitIntoString(String a);
        int OddIndexSum(int[] matrix_array);
        int EvenIndexSum(int[] matrix_array);

}
